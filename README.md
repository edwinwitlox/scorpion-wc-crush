# Scorpion WC Crush
Dit spel is een variant op het wereldberoemde bejeweled. Het originele spel is een nieuw leven ingeblazen door varianten als CandyCrush.
Aangezien wij voor een proof-of-concept een aantal relatief simpele apps nodig hadden werden er een aantal spellen ontwikkeld. Een van deze spellen van dit Crush spel. 

------
##Releasenotes:
### 1.2.0
- Implementatie van Ionic voor betere gebruikerservaring op diverse platformen

### 1.1.0 - 04-09-2014:
- Naam aangepast wegens plagiaat volgens de makers van Monster High
- Naam aangepast wegens plagiaat volgens de makers van Skylanders
- Zowel Skylander Crush als Monster High Crush werden uit de Google Play Store verwijderd

### 1.0.8 - 19-08-2014:
- Minimale android versie opgehoogd naar 11 voor tablet ondersteuning

### 1.0.7 - 19-08-2014:
- Allerlei icons en splashscreens toegevoegd voor android

### 1.0.6 - 19-08-2014:
- Fix voor overlay formaat van Game Over bericht

### 1.0.5 - 19-08-2014:
- Nieuwe Phonegap versie
- Animaties versoepeld
- Afbeeldingen en iconen aangepast aan het thema "Skylanders Crush"
- Achtergrond gradient aangepast
- Fix voor overlag formaat van game over bericht
- Aangeboden aan Google Play Store

### 1.0.4 - 02-05-2013:
- Extra icons en splashscreens toegevoegd
- Afbeeldingen en iconen aangepast aan het thema "Ordina Crush"
- App ID aangepast voor distributie

### 1.0.3 - 25-04-2013:
- Titel duidelijker gemaakt
- Snelheids winst door nieuwe versie van PhoneGap

### 1.0.2 - 22-04-2013:
- Afbeeldingen zijn duidelijker
- Verbeterde functie naamgeving
- Valsnelheid aangepast
- Verduidelijking van lettertypes
- Optimalisatie van de configuartie settings per device
- Aangepaste icons voor blackberry

### 1.0.1:
- Iconen zijn ontwikkeld
- Configuratie XML aangemaakt

### 1.0.0:
- Game ontwikkeld