$(function() { // DOM ready
    $(window).resize(on_resize);
    $('.icon').live('dragstart', function(e) {
        // prevent image dragging
        e.preventDefault();
    });

    $('#zone_message').live('touchmove mousemove', function(e) {
        // prevent window scrolling
        e.preventDefault();
    });

    $('.icon').live('touchstart mousedown', function(e) {

        if (!displacement_current && !displacement_prohibited) {
            dragmove = false;
            $icon = $(this);
            $icon.css('z-index', 20);
            icon_row = Number($icon.attr('data-row'));
            icon_col = Number($icon.attr('data-col'));
            if (e.originalEvent.type == 'touchstart') {
                doigt_init_x = e.originalEvent.touches[0].clientX;
                doigt_init_y = e.originalEvent.touches[0].clientY;
            }
            if (e.originalEvent.type == 'mousedown') {
                doigt_init_x = e.originalEvent.clientX;
                doigt_init_y = e.originalEvent.clientY;
            }
            displacement_current = true;
        }

    });

    $('#zone_game').live('touchmove mousemove', function(e) {
        // prevent window scrolling
        e.preventDefault();

        if (displacement_current) {

            var distance_x, distance_y;

            if (e.originalEvent.type == 'touchmove') {
                distance_x = e.originalEvent.touches[0].clientX - doigt_init_x;
                distance_y = e.originalEvent.touches[0].clientY - doigt_init_y;
            }
            if (e.originalEvent.type == 'mousemove') {
                distance_x = e.originalEvent.clientX - doigt_init_x;
                distance_y = e.originalEvent.clientY - doigt_init_y;
            }

            if (Math.abs(distance_x) > Math.abs(distance_y)) {
                if (distance_x > ICON_SIZE / 2) {
                    // right
                    if (icon_col < NR_COLUMNS - 1) {
                        dragmove = true;
                        $('.icon').removeClass('click adjacent');
                        deplacement(icon_row, icon_col, icon_row, icon_col + 1);
                    }
                }

                if (distance_x < -ICON_SIZE / 2) {
                    // left
                    if (icon_col > 0) {
                        dragmove = true;
                        $('.icon').removeClass('click adjacent');
                        deplacement(icon_row, icon_col, icon_row, icon_col - 1);
                    }
                }
            } else {
                if (distance_y > ICON_SIZE / 2) {
                    // down
                    if (icon_row < NR_LINES - 1) {
                        dragmove = true;
                        $('.icon').removeClass('click adjacent');
                        deplacement(icon_row, icon_col, icon_row + 1, icon_col);
                    }
                }

                if (distance_y < -ICON_SIZE / 2) {
                    // up
                    if (icon_row > 0) {
                        dragmove = true;
                        $('.icon').removeClass('click adjacent');
                        deplacement(icon_row, icon_col, icon_row - 1, icon_col);
                    }
                }
            }
        }
    });

    $('#zone_game').live('touchend mouseup', function(e) {
        if (displacement_current) {
            displacement_current = false;
            $icon.css('z-index', 10);
            if (!dragmove) {
                verif_click($icon);
            }
        }
    });

    $('.bt_new_game').live('click', function() {
        init_game();
    });

    on_resize();

    // wait until every image is loaded to launch the game
    loadimages(images, function() {
        init_game();
    });

    // tabs and panels
    $('.panel').hide();
    $('.tab').click(function() {
        var $this = $(this);
        $('.tab').removeClass('on');
        $this.addClass('on');
        $('.panel').hide();
        $('#' + $this.attr('data-target')).show();
    });
    $('.tab:first').click();

});